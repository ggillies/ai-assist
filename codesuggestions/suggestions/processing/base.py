import json
from pathlib import Path
from typing import Any, Optional, NamedTuple
from abc import ABC, abstractmethod

from codesuggestions.suggestions.processing.ops import LanguageId, lang_from_filename

from prometheus_client import Counter

__all__ = [
    "MetadataCodeContent",
    "MetadataImports",
    "MetadataPromptBuilder",
    "ModelEngineOutput",
    "MetadataModel",
    "ModelEngineBase",
]

LANGUAGE_COUNTER = Counter('code_suggestions_prompt_language', 'Language count by number', ['lang', 'extension'])

CODE_SYMBOL_COUNTER = Counter('code_suggestions_prompt_symbols', 'Prompt symbols count', ['lang', 'symbol'])


class MetadataCodeContent(NamedTuple):
    length: int
    length_tokens: int


class MetadataImports(NamedTuple):
    pre: MetadataCodeContent
    post: MetadataCodeContent


class MetadataPromptBuilder(NamedTuple):
    prefix: MetadataCodeContent
    suffix: MetadataCodeContent
    imports: Optional[MetadataImports] = None


class MetadataModel(NamedTuple):
    name: str
    engine: str


class ModelEngineOutput(NamedTuple):
    text: str
    model: MetadataModel
    lang_id: Optional[LanguageId] = None
    metadata: Optional[MetadataPromptBuilder] = None

    def lang(self) -> str:
        return self.lang_id.name.lower() if self.lang_id else ""


class ModelEngineBase(ABC):
    async def generate_completion(self, prefix: str, suffix: str, file_name: str, **kwargs: Any) -> ModelEngineOutput:
        lang_id = lang_from_filename(file_name)
        self.increment_lang_counter(file_name, lang_id)
        return await self._generate_completion(prefix, suffix, file_name, lang_id, **kwargs)

    def increment_lang_counter(self, filename: str, lang_id: Optional[LanguageId] = None):
        labels = {'lang': None}

        if lang_id:
            labels['lang'] = lang_id.name.lower()

        labels['extension'] = Path(filename).suffix[1:]

        LANGUAGE_COUNTER.labels(**labels).inc()

    @abstractmethod
    async def _generate_completion(self, prefix: str, suffix: str, file_name: str, lang_id: LanguageId, **kwargs: Any) -> ModelEngineOutput:
        pass

    def increment_code_symbol_counter(self, lang_id: LanguageId, symbol_map: dict):
        for symbol, count in symbol_map.items():
            CODE_SYMBOL_COUNTER.labels(lang=lang_id.name.lower(), symbol=symbol).inc(count)

    @staticmethod
    def _read_json(filepath: Path) -> dict[str, list]:
        with open(str(filepath), "r") as f:
            return json.load(f)
