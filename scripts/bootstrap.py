#!/usr/bin/env python

from codesuggestions.tokenizer import init_tokenizer


if __name__ == "__main__":
    init_tokenizer()
