apiVersion: batch/v1
kind: Job
metadata:
  name: model-loader-job
spec:
  ttlSecondsAfterFinished: 100
  template:
    spec:
      imagePullSecrets:
        - name: gitlab-registry
      containers:
        - name: model-loader
          image: registry.gitlab.com/gitlab-org/modelops/applied-ml/code-suggestions/ai-assist/tmp/model-fauxpilot:e8276d47
          env:
            - name: MODEL_NUM_GPU
              value: "2"
            - name: MODEL_ARCHIVE_NAME
              value: "$(MODEL_NUM_GPU)-gpu.tar.zst"
            - name: MODEL_ARCHIVE
              value: "/data/$(MODEL_NUM_GPU)-gpu.tar.zst"
            - name: MODELS_DIR
              value: /data/models
            - name: MODEL_WEIGHTS_DIR
              value: "/data/models/fastertransformer/1/$(MODEL_NUM_GPU)-gpu"
            - name: MODEL_FT_CONFIG
              value: /data/models/fastertransformer/config.pbtxt
            # Change PIPELINE_PARA_SIZE to 2 when starting this job in the stg cluster
            - name: PIPELINE_PARA_SIZE
              value: "1"
            - name: TENSOR_PARA_SIZE
              value: "2"

            # Set SKIP_FT_WEIGHTS_DOWNLOAD to one to skip the download
            - name: SKIP_FT_WEIGHTS_DOWNLOAD
              value: "0"
          command: ["/bin/sh", "-x", "-c", "--"]
          args:
            - |
              set -euo pipefail

              apk add --no-cache python3

              # Install gcloud (TODO: move this into the Docker image)
              curl https://dl.google.com/dl/cloudsdk/release/google-cloud-sdk.tar.gz > /tmp/google-cloud-sdk.tar.gz
              mkdir -p /usr/local/gcloud
              tar -C /usr/local/gcloud -xvf /tmp/google-cloud-sdk.tar.gz >/dev/null
              /usr/local/gcloud/google-cloud-sdk/install.sh
              export PATH=$PATH:/usr/local/gcloud/google-cloud-sdk/bin

              echo "Performing gcloud auth...";
              gcloud auth activate-service-account --key-file=/etc/gcp-storage-credentials/key.json;

              mkdir -p /data/downloading
              download_dir=$(mktemp -d -p /data/downloading)
              trap "rm -rf ${download_dir}" EXIT

              downloaded_weights_file=${download_dir}/${MODEL_ARCHIVE_NAME}

              if [ "${SKIP_FT_WEIGHTS_DOWNLOAD-}" != "1" ]; then
                echo "Copying model from GCS...";
                gsutil -m cp "gs://code-suggestions/model-deployments/codegen-v2/16b-multi/${MODEL_ARCHIVE_NAME}" "${downloaded_weights_file}";

                rm -rf "${MODELS_DIR}" && mkdir -p "${MODELS_DIR}";
                rm -rf "${MODEL_WEIGHTS_DIR}" && mkdir -p "${MODEL_WEIGHTS_DIR}";
              fi

              echo "Copy complete, processing model...";
              cp -r /model/* "${MODELS_DIR}";
              sed -e 's@${MODELS_DIR}@'"$MODELS_DIR"'@' \
                 -e 's@${MODEL_NUM_GPU}@'"$MODEL_NUM_GPU"'@' \
                 -e 's@${PIPELINE_PARA_SIZE}@'"$PIPELINE_PARA_SIZE"'@' \
                 -e 's@${TENSOR_PARA_SIZE}@'"$TENSOR_PARA_SIZE"'@' \
                 "${MODEL_FT_CONFIG}" > "${MODEL_FT_CONFIG}.tmp";
              mv "${MODEL_FT_CONFIG}.tmp" "${MODEL_FT_CONFIG}";

              if [ "${SKIP_FT_WEIGHTS_DOWNLOAD-}" != "1" ]; then
                zstd -dc "${downloaded_weights_file}" | tar -xf - -C "${MODEL_WEIGHTS_DIR}" --strip-components=1;
              fi

              echo "Copy complete..."
          volumeMounts:
            # Shared volume containing downloaded models
            - name: model-storage-volume
              mountPath: /data
            - name: gcp-storage-credentials
              mountPath: "/etc/gcp-storage-credentials"
              readOnly: true
      volumes:
        - name: model-storage-volume
          persistentVolumeClaim:
            claimName: model-storage-pvc
        - name: gcp-storage-credentials
          secret:
            secretName: gcp-storage-credentials
      restartPolicy: Never
